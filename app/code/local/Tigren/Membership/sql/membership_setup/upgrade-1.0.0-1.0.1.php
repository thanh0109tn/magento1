<?php
$installer = $this;
$installer->startSetup();
$installer->run("
CREATE TABLE IF NOT EXISTS  membership_customer (
package_id INT(10) UNSIGNED NOT NULL,
customer_id INT(10) UNSIGNED NOT NULL,

CONSTRAINT fk_package_id FOREIGN KEY (package_id)
REFERENCES membership_package(id)
ON DELETE CASCADE
ON UPDATE CASCADE,
CONSTRAINT fk_customer_id FOREIGN KEY (customer_id)
REFERENCES customer_entity(entity_id)
ON DELETE CASCADE
ON UPDATE CASCADE
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8");
$installer->endSetup();