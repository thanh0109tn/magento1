<?php
class Tigren_Membership_Model_Resource_Package extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('membership/package', 'id');
    }

    protected function _afterLoad(Mage_Core_Model_Abstract $object)
    {
        $customer = $object->getData();

//        var_dump($customer);die;
        return parent::_afterLoad($object);
    }

    protected function _afterSave(Mage_Core_Model_Abstract $object)
    {

        $customers = $object->getCustomers();
//        var_dump($customer);die;
        $write = $this->_getWriteAdapter();
        if($customers) {
            $condition = array('package_id = ?' => $object->getId());
                $write->delete( $this->getTable('membership/customer'), $condition);
                $customers = explode('&',$customers);
//            $data = new Varien_Object();
//        var_dump($data);die;
            foreach ($customers as $customer)
            {
                $data = ['customer_id' => $customer, 'package_id' => $object->getId()];
            }
            $write->insert($this->getTable('membership/customer'), $data);
        }
        return $this;
    }
    public function getCustomer($Id) {
        $adapter = $this->_getReadAdapter();
        $select  = $adapter->select()
            ->from($this->getTable('membership/customer'), 'customer_id')
            ->where('package_id = ?',(int)$Id);
//        var_dump($select); die('abc');
        return $adapter->fetchCol($select);
    }
}