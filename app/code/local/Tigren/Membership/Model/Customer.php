<?php
class Tigren_Membership_Model_Customer extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('membership/customer');
    }
}