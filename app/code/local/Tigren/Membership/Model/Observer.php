<?php
class Tigren_Membership_Model_Observer
{
    public function changePrice($observer) {
        $product = $observer->getEvent()->getProduct();
        $customerData = Mage::getSingleton('customer/session')->getCustomer();
        $customerLogin = Mage::getSingleton('customer/session')->isLoggedIn();
        $customerId =  Mage::getModel('membership/customer')->load($customerData->getId());
        $package = Mage::getModel('membership/package')->load($customerId->getData('package_id'));
//        var_dump($customerId);die;
        if($customerLogin){
            if($customerData->getId()) {
//                var_dump($customerId);die;
                if (empty($package->getData('discount_value')))
                {
                    return $product->getFinalPrice();
                }
                elseif($package->getData('discount_type') == 1){
//                    echo "<pre>";var_dump($package);die;
                    $discountValue = $package->getData('discount_value');
//                    echo "<pre>";var_dump($discountValue);die;
                    $price = (float)$product->getFinalPrice() - (float)$product->getFinalPrice() * ((float)$discountValue/100);
                    $newPrice = $product->setFinalPrice($price);
                    return $newPrice;
                }
                else{
                    $price = (float)$product->getPrice() - 5;
                    $newPrice = $product->setPrice($price);
                    return $newPrice;
                }
            }
        }
        return $this;
    }
    public function changePriceCollection($observer) {
        $collection = $observer->getCollection();
        $customerData = Mage::getSingleton('customer/session')->getCustomer();
        $customerLogin = Mage::getSingleton('customer/session')->isLoggedIn();
        $customerId =  Mage::getModel('membership/customer')->load($customerData->getId());
        $package = Mage::getModel('membership/package')->load($customerId->getData('package_id'));
        if($customerLogin){
            foreach ($collection as $product){
                if($customerData->getId()) {
                    if (empty($package->getData('discount_value'))) {
                        return $product->getFinalPrice();
                    }
                    else if($package->getData('discount_type') == 1) {
                        $discountValue = $package->getData('discount_value');
                        $price = (float)$product->getFinalPrice() - (float)$product->getFinalPrice() * ((float)$discountValue/100);
                        $newPrice = $product->setFinalPrice($price);
                        return $newPrice;
                    }
                    else{
                        $price = (float)$product->getFinalPrice() - 5;
                        $newPrice = $product->setFinalPrice($price);
                        return $newPrice;
                    }
                }
            }
        }
        return $this;
    }
}