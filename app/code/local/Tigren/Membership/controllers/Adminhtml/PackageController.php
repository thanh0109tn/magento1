<?php
class Tigren_Membership_Adminhtml_PackageController extends Mage_Adminhtml_Controller_Action
{
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('membership/package');
    }

    public function indexAction()
    {
        $this->loadLayout();
        $this->_title($this->__("MemberShip"));
        $this->renderLayout();
    }

    public function editAction()
    {
        $packageId     = $this->getRequest()->getParam('id');
        $packageModel  = Mage::getModel('membership/package')->load($packageId);
        if($packageModel->getId() || $packageId == 0) {

        Mage::register('package_data', $packageModel);

            $this->loadLayout();
            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock('membership/adminhtml_package_edit'))
                ->_addLeft($this->getLayout()->createBlock('membership/adminhtml_package_edit_tabs'));
            $this->renderLayout();
        } else {
        Mage::getSingleton('adminhtml/session')->addError('Item does not exist');
        $this->_redirect('*/*/');
        }
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function saveAction()
    {
        if ($this->getRequest()->getPost())
        {
            try {
                $postData = $this->getRequest()->getPost();
//                Zend_debug::dump($postData);die;
                $packageModel = Mage::getModel('membership/package');
//                Zend_debug::dump($packageModel);die;
//                die("asdsa");
                if( $this->getRequest()->getParam('id') ) {

                    $customer = $this->getRequest()->getParam('customer_ids');
//                    die("asdsa");
                    if (isset($customer) ) {
//                       var_dump($customer);die();
                    $packageModel
                        ->addData($postData)
                        ->setId($this->getRequest()->getParam('id'))
                        ->setCustomers($postData['customer_ids'])

                        ->save();
                    Mage::getSingleton('adminhtml/session')->addSuccess('successfully saved');
                    Mage::getSingleton('adminhtml/session')->setpackageData(false);
                    $this->_redirect('*/*/');
                    }
                }
                else{
                    $packageModel
                        ->addData($postData)
//                        ->setId($this->getRequest()->getParam('id'))
                        ->setCustomers($postData['customer_ids'])
                        ->save();
                    Mage::getSingleton('adminhtml/session')->addSuccess('successfully saved');
                    Mage::getSingleton('adminhtml/session')->setpackageData(false);
                    $this->_redirect('*/*/');
                }

            } catch (Exception $e) {

                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setpackageData($this->getRequest()->getPost());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));

            }
            $this->_redirect('*/*/');
            return;
        }
    }

    public function deleteAction()
    {
        if($this->getRequest()->getParam('id') > 0)
        {
            try
            {
                $gridModel = Mage::getModel('membership/package');
                $gridModel->setId($this->getRequest()->getParam('id'))->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess('successfully deleted');
                $this->_redirect('*/*/');
            }
            catch (Exception $e)
            {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }

    public function massDeleteAction()
    {
        $requestIds = $this->getRequest()->getParam('id');
        if (!is_array($requestIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select request(s)'));
        } else {
            try {
                foreach ($requestIds as $requestId) {
                    $RequestData = Mage::getModel('membership/package')->load($requestId);
                    $RequestData->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully deleted', count($requestIds)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/');
    }

    public function customerAction(){
        $this->loadLayout();
        $this->getLayout()->getBlock('customer.grid')
            ->setCustomers($this->getRequest()->getPost('customers', null));
        $this->renderLayout();
    }

    public function customergridAction(){
        $this->loadLayout();
        $this->getLayout()->getBlock('customer.grid')
            ->setCustomers($this->getRequest()->getPost('customers', null));
        $this->renderLayout();
    }
}