<?php
class Tigren_Membership_Block_Adminhtml_Package extends Mage_Adminhtml_Block_Widget_Grid_Container {
    public function __construct()
    {
//        die("abc");
        $this->_blockGroup = 'membership';
        $this->_controller = 'adminhtml_package';
        $this->_headerText = $this->__('Package');

        parent::__construct();
    }
}