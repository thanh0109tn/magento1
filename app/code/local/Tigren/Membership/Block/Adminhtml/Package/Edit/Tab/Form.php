<?php
class Tigren_Membership_Block_Adminhtml_Package_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('package_form',
            array('legend'=>'information'));
        $fieldset->addField('package_name', 'text',
            array(
                'label' => 'Package Name',
                'name' => 'package_name',
                'required' => true
            ));
        $fieldset->addField('discount_type', 'select',
            array(
                'label' => 'Discount Type',
                'name' => 'discount_type',
                'required' => true,
                'options'  => array(
                    '1' => Mage::helper('adminhtml')->__('Percent'),
                    '0' => Mage::helper('adminhtml')->__('Fixed'),
                )
            ));
        $fieldset->addField('discount_value', 'text',
            array(
                'label' => 'Discount Value',
                'name' => 'discount_value',
                'required' => true
            ));

        if ( Mage::registry('package_data') )
        {
            $form->setValues(Mage::registry('package_data')->getData());
        }

        return parent::_prepareForm();
    }
}
