<?php
class Tigren_Membership_Block_Adminhtml_Package_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setDefaultSort('id');
        $this->setId('membership_package_package');
        $this->setDefaultDir('asc');
        $this->setSaveParametersInSession(true);
    }
    protected function _getCollectionClass()
    {
        return 'membership/package_collection';
    }
    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel($this->_getCollectionClass());
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
    protected function _prepareColumns()
    {
        $this->addColumn('id',
            array(
                'header'=> $this->__('ID'),
                'width' => '50px',
                'index' => 'id'
            )
        );
        $this->addColumn('package_name',
            array(
                'header'=> $this->__('Package Name'),
                'index' => 'package_name'
            )
        );
        $this->addColumn('discount_type',
            array(
                'header'=> $this->__('Discount Type'),
                'index' => 'discount_type',
                'type'     => 'options',
                'options'  => array(
                    1 => Mage::helper('adminhtml')->__('Percent'),
                    0 => Mage::helper('adminhtml')->__('Fixed')
                ),
            )
        );
        $this->addColumn('discount_value',
            array(
                'header'=> $this->__('Discount Value'),
                'index' => 'discount_value'
            )
        );
        return parent::_prepareColumns();
    }
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('id');
        $this->getMassactionBlock()->addItem('delete', array(
            'label'    => Mage::helper('core')->__('Delete'),
            'url'      => $this->getUrl('*/*/massDelete'),
            'confirm'  => Mage::helper('core')->__('Are you sure?')
        ));

        return $this;
    }
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

}