<?php
class Tigren_Membership_Block_Adminhtml_Package_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('package_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle('Information');
    }

    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
            'label' => 'Package',
            'title' => 'Package',
            'content' => $this->getLayout()
                ->createBlock('membership/adminhtml_package_edit_tab_form')
                ->toHtml()
        ));
        $this->addTab('customer_section', array(
            'label'     => Mage::helper('membership')->__('Customer'),
            'title'     => Mage::helper('membership')->__('Customer'),
            'url'       => $this->getUrl('*/*/customer', array('_current' => true)),
            'class'     => 'ajax',
        ));

        return parent::_beforeToHtml();
    }
}