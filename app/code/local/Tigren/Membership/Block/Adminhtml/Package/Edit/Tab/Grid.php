<?php
class Tigren_Membership_Block_Adminhtml_Package_Edit_Tab_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('customer_grid');
        $this->setUseAjax(true); //You need to write this as we are using Ajax Grid
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir("ASC");
//        if($this->getRequest()->getParams('id')){
//        $this->setDefaultFilter(array('in_customers' => 1)); /*By default we have added a filter for the rows, that in_customers value to be 1*/
//        }
        $this->setSaveParametersInSession(false); /*Saving parameters in session creates problems so we making it false*/
    }

    /* This will be set your collection. Please ignore the code written inside here nothing is new in this*/
    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('customer/customer_collection');
        $idPackage = $this->getRequest()->getParam('id');
//        echo'<pre>';    var_dump($idPackage);die;
        $list = [];
        if(empty($idPackage)) {
            $packageCustomers = Mage::getModel('membership/customer')->getCollection();
            foreach ($packageCustomers as $customerInPackage)
            {
                $list[] = $customerInPackage->getData('customer_id');
            }
        }
        else{
            $packageCustomers = Mage::getModel('membership/customer')->getCollection()
                ->addFieldToFilter('package_id', array('neq' => $idPackage)) ;
            foreach ($packageCustomers as $customerInPackage)
            {
                $list[] = $customerInPackage->getData('customer_id');
            }
            var_dump($list);
        }
////        echo'<pre>';    var_dump($packageCustomers);die;
//
////        echo'<pre>';    var_dump($listCustomerRegister);die;
//        if(empty($idPackage)) {
//            $collection->addNameToSelect()
//                ->addAttributeToSelect('entity_id')
//                ->addAttributeToSelect('email')
//                ->addAttributeToSelect('name')
////                ->addAttributeToFilter('entity_id', (array('nin' => $list)))
//            ;
//        }
//        else{
//            $packageCustomer = Mage::getModel('membership/customer')->getCollection();
////            foreach ($packageCustomer as $customerInPackage)
////            {
////                $list[] = $customerInPackage->getData('customer_id');
//////                var_dump($list);die;
////            }
////            var_dump($list);die;
//            $packageCustomers = Mage::getModel('membership/customer')->getCollection()
//                ->addFilter('package_id', array('nin' => $idPackage)) ;
//            foreach ($packageCustomers as $customersInPackage)
//            {
//                $listCustomerRegister[] = $customersInPackage->getData('customer_id');
////                var_dump($listCustomerRegister);die;
//            }
////            var_dump($listCustomerRegister);die;
//            $collection->addNameToSelect()
//                ->addAttributeToSelect('entity_id')
//                ->addAttributeToSelect('email')
//                ->addAttributeToSelect('name')
//                ->addAttributeToFilter('entity_id',
//                    (array('in' => $listCustomerRegister)));
////                    ->addAttributeToFilter('entity_id',
////                        (array('in' => $list)));
//
        $collection->addNameToSelect()
            ->addAttributeToSelect('entity_id')
            ->addAttributeToSelect('email')
            ->addAttributeToSelect('name');
        if(!empty($list)){


            $collection->addAttributeToFilter('entity_id', (array('nin' => $list)));
        }else{

        }
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /*This is for the custom filter for the checkboxes we are showing*/
    protected function _addColumnFilterToCollection($column)
    {
        if ($column->getId() == 'in_customers') {
            $ids = $this->_getSelectedCustomers();
            if (empty($ids)) {
                $ids = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('entity_id', array('in'=>$ids));
            } else {
                if($ids) {
                    $this->getCollection()->addFieldToFilter('entity_id', array('nin'=>$ids));
                }
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }

    protected function _prepareColumns()
    {
        /*Column to show checkbox*/
        $this->addColumn('in_customers', array(
            'header_css_class'  => 'a-center',
            'type'              => 'checkbox',
            'name'              => 'customer_ids',
            'values'            => $this->_getSelectedCustomers(), //Function to get selected customer's id
            'align'             => 'center',
            'index'             => 'entity_id',
        ));
        $this->addColumn('entity_id', array(
            'header'    => Mage::helper('customer')->__('ID'),
            'width'     => '50px',
            'index'     => 'entity_id',
            'type'  => 'number',
        ));
        $this->addColumn('name', array(
            'header'    => Mage::helper('customer')->__('Name'),
            'index'     => 'name'
        ));
        $this->addColumn('email', array(
            'header'    => Mage::helper('customer')->__('Email'),
            'index'     => 'email'
        ));
        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->_getData('grid_url') ? $this->_getData('grid_url') : $this->getUrl('*/*/customergrid', array('_current'=>true));
    }

    protected function _getSelectedCustomers()   // Used in grid to return selected customers values.
    {
        $customers = array_keys($this->getSelectedCustomers());
        return $customers;
    }

    public function getSelectedCustomers(){
        $tm_id = $this->getRequest()->getParam('id');
//        var_dump($tm_id);die;
        if(!isset($tm_id)) {
            $tm_id = 0;
        }
        $conn = Mage::getModel('membership/customer')->load($tm_id,'package_id');
//        echo "<pre>";var_dump($conn);die;
        $data =  $conn->getData('customer_id');
//        var_dump($data);die;
        $data = explode(',',$data);
        $customers = $data;
//        var_dump($data);die;
        $customerIds = array();
        foreach($customers as $customer) {
            $customerIds[$customer] = array('customer_id'=>$customer);
        }
        return $customerIds;
    }
}