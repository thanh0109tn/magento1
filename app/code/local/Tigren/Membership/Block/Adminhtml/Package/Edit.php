<?php

class Tigren_Membership_Block_Adminhtml_Package_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
        $this->_objectId = 'id';
        $this->_blockGroup = 'membership';
        $this->_controller = 'adminhtml_package';
        $this->_updateButton('save', 'label','Save');
        $this->_updateButton('delete', 'label', 'Delete');
    }

    public function getHeaderText()
    {
        if( Mage::registry('package_data') && Mage::registry('package_data')->getId() )
        {
            return 'Edit '.$this->htmlEscape( Mage::registry('package_data')->getTitle() );
        }
        else
        {
            return 'Add new';
        }
    }
}